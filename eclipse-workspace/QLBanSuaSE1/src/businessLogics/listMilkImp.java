package businessLogics;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javaBeans.CtHoadon;
import javaBeans.HangSua;
import javaBeans.LoaiSua;
import javaBeans.Sua;
import utils.HibernateFactory;

public class listMilkImp implements ListMilk {

	@Override
	public List<Sua> dss() {
		Session ss = HibernateFactory.getsF().openSession();
		CriteriaBuilder a = ss.getCriteriaBuilder();
		CriteriaQuery<Sua> b = a.createQuery(Sua.class);
		b.from(Sua.class);
		List<Sua> c = ss.createQuery(b).getResultList();
		ss.close();
		return c;
	}

	@Override
	public List<HangSua> dshs() {
		Session ss = HibernateFactory.getsF().openSession();
		CriteriaBuilder a = ss.getCriteriaBuilder();
		CriteriaQuery<HangSua> query = a.createQuery(HangSua.class);
		query.from(HangSua.class);
		List<HangSua> b = ss.createQuery(query).getResultList();
		ss.close();
		return b;
	}

	@Override
	public List<LoaiSua> dsls() {
		SessionFactory factory = HibernateFactory.getsF();
		Session ss = factory.openSession();
		CriteriaBuilder cb = ss.getCriteriaBuilder();
		CriteriaQuery<LoaiSua> ls = cb.createQuery(LoaiSua.class);
		ls.from(LoaiSua.class);
		List<LoaiSua> lss = ss.createQuery(ls).getResultList();
		ss.close();
		return lss;
	}

	@Override
	public List<Sua> getSuatheoHangsua(String hs) {
		Session ss = HibernateFactory.getsF().openSession();
		CriteriaBuilder builder = ss.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		Root<Sua> root = query.from(Sua.class);
		Root<HangSua> rootHangSua = query.from(HangSua.class);
		Predicate pre1 = builder.equal(root.get("hangSua"), rootHangSua.get("maHangSua"));
		Predicate pre2 = builder.equal(rootHangSua.get("maHangSua"), hs);
		query.select(root).where(builder.and(pre1, pre2));
		List<Sua> sua = ss.createQuery(query).getResultList();
		return sua;
	}

	@Override
	public List<Sua> getSuatheoLoaisua(String ls) {
		Session ss = HibernateFactory.getsF().openSession();
		CriteriaBuilder builder = ss.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		Root<Sua> root = query.from(Sua.class);
		Root<LoaiSua> rootLoaiSua = query.from(LoaiSua.class);
		Predicate pre1 = builder.equal(root.get("loaiSua"), rootLoaiSua.get("maLoaiSua"));
		Predicate pre2 = builder.equal(rootLoaiSua.get("maLoaiSua"), ls);
		query.select(root).where(builder.and(pre1, pre2));
		List<Sua> sua = ss.createQuery(query).getResultList();
		return sua;
	}

	@Override
	public List<Sua> getSuatheoHangLoaiTenSua(String hs, String ls, String tenSua) {
		Session session = HibernateFactory.getsF().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		Root<Sua> root = query.from(Sua.class);
		Root<HangSua> rootHangSua = query.from(HangSua.class);
		Root<LoaiSua> rootLoaiSua = query.from(LoaiSua.class);

		Predicate pre1 = builder.equal(root.get("hangSua"), rootHangSua.get("maHangSua"));
		Predicate pre2 = builder.equal(root.get("loaiSua"), rootLoaiSua.get("maLoaiSua"));
		Predicate pre3 = builder.equal(rootHangSua.get("maHangSua"), hs);
		Predicate pre4 = builder.equal(rootLoaiSua.get("maLoaiSua"), ls);
		Predicate pre5 = builder.like(root.get("tenSua").as(String.class), "%" + tenSua + "%");

		query.select(root).where(builder.and(pre1, pre2, pre3, pre4, pre5));
		List<Sua> sua1 = session.createQuery(query).getResultList();
		session.close();
		return sua1;
	}

	@Override
	public List<Sua> getSuaBanChay() {
		Session session = HibernateFactory.getsF().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Object[]> query = builder.createQuery(Object[].class);
		Root<Sua> root = query.from(Sua.class);
		Root<CtHoadon> rootCtHoaDon = query.from(CtHoadon.class);
		query.multiselect(root,builder.sum(rootCtHoaDon.<Integer>get("soLuong")))
				.where(builder.equal(root.get("maSua"), rootCtHoaDon.get("sua")))
				.groupBy(root.get("maSua"))
				.orderBy(builder.desc(builder.sum(rootCtHoaDon.<Integer>get("soLuong"))));
		List<Object[]> ob = session.createQuery(query).setMaxResults(5).getResultList();
		List<Sua> sua = new ArrayList<>();
		for(Object[] obj:ob)
		{
			sua.add((Sua) obj[0]);
		}
		session.close();
		return sua;
	}

	@Override
	public Sua getSuaById(String id) {
		Session session = HibernateFactory.getsF().openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		Root<Sua> root = query.from(Sua.class);
		query.select(root).where(builder.equal(root.get("maSua"), id));
		Sua sua = session.createQuery(query).getSingleResult();
		session.close();
		return sua;
	}

}
