package businessLogics;

import javaBeans.HangSua;
import javaBeans.LoaiSua;
import javaBeans.Sua;

public interface FindMilkById {
	HangSua findById(String id);
	LoaiSua findById1(String id);
	Sua findById2(String id);
	
}
