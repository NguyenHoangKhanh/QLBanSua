package businessLogics;

import java.util.List;

import javaBeans.HangSua;
import javaBeans.Sua;
import javaBeans.LoaiSua;

public interface ListMilk {
	List<Sua> dss();
	List<HangSua> dshs();
	List<LoaiSua> dsls();
	List<Sua> getSuatheoHangsua(String hs);
	List<Sua> getSuatheoLoaisua(String ls);
	List<Sua> getSuatheoHangLoaiTenSua(String hs, String ls,String tenSua);
	List<Sua> getSuaBanChay();
	Sua getSuaById(String id);
}
