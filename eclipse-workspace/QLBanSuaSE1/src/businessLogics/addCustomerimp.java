package businessLogics;

import org.hibernate.Session;
import org.hibernate.Transaction;

import javaBeans.KhachHang;
import utils.HibernateFactory;

public class addCustomerimp implements AddCustomer {

	@Override
	public void addCustumer(KhachHang cus) {
		Session ss = HibernateFactory.getsF().openSession();
		Transaction trans = ss.beginTransaction();
		ss.persist(cus);
		trans.commit();
		ss.close();
	}

}
