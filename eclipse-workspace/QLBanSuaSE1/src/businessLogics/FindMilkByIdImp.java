package businessLogics;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javaBeans.HangSua;
import javaBeans.LoaiSua;
import javaBeans.Sua;
import utils.HibernateFactory;

public class FindMilkByIdImp implements FindMilkById {

	@Override
	public HangSua findById(String id) {
		Session ss = HibernateFactory.getsF().openSession();
		HangSua hs = ss.get(HangSua.class, id);
		ss.close();
		return hs;
	}

	@Override
	public LoaiSua findById1(String id) {
		Session ss = HibernateFactory.getsF().openSession();
		LoaiSua ls = ss.get(LoaiSua.class, id);
		ss.close();
		return ls;
	}

	@Override
	public Sua findById2(String id) {
		Session ss = HibernateFactory.getsF().openSession();
		CriteriaBuilder builder = ss.getCriteriaBuilder();
		CriteriaQuery<Sua> query = builder.createQuery(Sua.class);
		Root<Sua> root = query.from(Sua.class);
		query.select(root).where(builder.equal(root.get("maSua"), id));
		Sua sua = ss.createQuery(query).getSingleResult();
		return sua;
	}

}
