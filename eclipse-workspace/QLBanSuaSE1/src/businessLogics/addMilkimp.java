package businessLogics;

import org.hibernate.Session;
import org.hibernate.Transaction;

import javaBeans.Sua;
import utils.HibernateFactory;

public class addMilkimp implements AddMilk {

	@Override
	public void addMilk(Sua sua) {
		Session session = HibernateFactory.getsF().openSession();
		Transaction trans = session.beginTransaction();
		session.persist(sua);
		trans.commit();
		session.close();

	}

}
