package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businessLogics.FindMilkById;
import businessLogics.FindMilkByIdImp;
import businessLogics.ListMilk;
import businessLogics.listMilkImp;
import javaBeans.HangSua;
import javaBeans.LoaiSua;
import javaBeans.Sua;

@WebServlet("/TimKiemSuaServlet")
public class TimKiemSuaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TimKiemSuaServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		ListMilk lm = new listMilkImp();
		FindMilkById fm = new FindMilkByIdImp();

		request.setAttribute("dsls", lm.dsls());
		request.setAttribute("dshs", lm.dshs());

		if (request.getParameter("btnTimKiem") != null) {
			String hangSua = request.getParameter("cboHangSua");
			String loaiSua = request.getParameter("cboLoaiSua");
			String tenSua = request.getParameter("txtTenSuaTim");

			List<Sua> sua = lm.getSuatheoHangLoaiTenSua(hangSua, loaiSua, tenSua);
			request.setAttribute("lSua", sua);
			request.setAttribute("count", sua.size());
		}

		request.getRequestDispatcher("views/tim-kiem-sua.jsp").include(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
