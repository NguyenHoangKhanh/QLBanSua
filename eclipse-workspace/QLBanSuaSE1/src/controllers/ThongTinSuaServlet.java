package controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businessLogics.FindMilkById;
import businessLogics.FindMilkByIdImp;
import javaBeans.Sua;

@WebServlet("/ThongTinSuaServlet")
public class ThongTinSuaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ThongTinSuaServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		Sua sua;
		String maSua = request.getParameter("maSua");
		FindMilkById fm = new FindMilkByIdImp();
		if (maSua != null) {
			sua = fm.findById2(maSua);
			request.setAttribute("result", sua);
		}

		request.getRequestDispatcher("views/thong-tin-sua.jsp").include(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
