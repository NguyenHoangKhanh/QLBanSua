package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businessLogics.AddMilk;
import businessLogics.FindMilkById;
import businessLogics.FindMilkByIdImp;
import businessLogics.ListMilk;
import businessLogics.addMilkimp;
import businessLogics.listMilkImp;
import javaBeans.HangSua;
import javaBeans.LoaiSua;
import javaBeans.Sua;

@WebServlet("/ThemSuaMoiServlet")
public class ThemSuaMoiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ThemSuaMoiServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		FindMilkById fm = new FindMilkByIdImp();
		AddMilk am = new addMilkimp();
		ListMilk hs = new listMilkImp();
		List<LoaiSua> ls = hs.dsls();
		List<HangSua> hhs = hs.dshs();
		request.setAttribute("dshs", hhs);
		request.setAttribute("dsls", ls);
		if (request.getParameter("btnThemMoi") != null) {
			String maSua = request.getParameter("txtMaSua");
			String tenSua = request.getParameter("txtTenSua");
			String hangSua = request.getParameter("cboHangSua");
			String loaiSua = request.getParameter("cboLoaiSua");
			int trongLuong = Integer.parseInt(request.getParameter("txtTrongLuong"));
			int donGia = Integer.parseInt(request.getParameter("txtDonGia"));
			String tpDinhDuong = request.getParameter("txtTPDinhDuong");
			String loiIch = request.getParameter("txtLoiIch");
			String hinh = request.getParameter("txtHinh");

			HangSua hs1 = fm.findById(hangSua);
			LoaiSua ls1 = fm.findById1(loaiSua);
			Sua sua = new Sua(maSua, hs1, ls1, tenSua, trongLuong, donGia, tpDinhDuong, loiIch, hinh);
			am.addMilk(sua);
		}

		request.getRequestDispatcher("views/them-sua-moi.jsp").include(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
