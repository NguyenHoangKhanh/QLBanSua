package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businessLogics.FindMilkById;
import businessLogics.FindMilkByIdImp;
import businessLogics.ListMilk;
import businessLogics.listMilkImp;
import businessLogics.sumPage;
import javaBeans.HangSua;
import javaBeans.LoaiSua;
import javaBeans.Sua;

@WebServlet({ "/DanhSachSuaServlet" })
public class DanhSachSuaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DanhSachSuaServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		FindMilkById fm = new FindMilkByIdImp();
		ListMilk a = new listMilkImp();
		List<Sua> sua;
		sumPage sp = new sumPage();
		int sumPg = 0;
		int limit = 5;
		int start = 0;
		int size = 0;
		int numberPage = 1;
		if (request.getParameter("trang") != null) {

			numberPage = Integer.parseInt(request.getParameter("trang"));
		}

		if (request.getParameter("maHang") != null) {

			sua = a.getSuatheoHangsua(request.getParameter("maHang"));
		} else if (request.getParameter("maLoai") != null) {
			sua = a.getSuatheoLoaisua(request.getParameter("maLoai"));

		} else {
			sua = a.dss();
		}
		size = sua.size();
		start = sp.FindStart(numberPage, limit);
		sumPg = sp.sumPagee(limit, size);
		request.setAttribute("tongSoTrang", sumPg);
		if (numberPage == sumPg) {
			limit = sua.size() % limit;
		}
		request.setAttribute("dss", sp.danhSachSuaPhanTrang(sua, limit, start));
		request.setAttribute("dsls", a.dsls());
		request.setAttribute("dshs", a.dshs());
		request.getRequestDispatcher("views/danh-sach-sua.jsp").include(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
