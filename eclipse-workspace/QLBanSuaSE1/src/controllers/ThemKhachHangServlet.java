package controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businessLogics.AddCustomer;
import businessLogics.addCustomerimp;
import javaBeans.KhachHang;

@WebServlet("/ThemKhachHangServlet")
public class ThemKhachHangServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ThemKhachHangServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		KhachHang cus;
		AddCustomer ac = new addCustomerimp();
		String button, ma, ten, phai, diachi, dienthoai, email;
		boolean phaii;
		button = request.getParameter("btnThemMoi");
		ma = request.getParameter("txtMaKH");
		ten = request.getParameter("txtTenKH");
		phai = request.getParameter("rdbPhai");
		if (phai == "Nam") {
			phaii = true;
		} else {
			phaii = false;
		}
		diachi = request.getParameter("txtDiaChi");
		dienthoai = request.getParameter("txtDienThoai");
		email = request.getParameter("txtEmail");
		if (button != null) {
			cus = new KhachHang(ma, ten, phaii, diachi, dienthoai, email);
			ac.addCustumer(cus);
		}
		request.getRequestDispatcher("views/them-khach-hang.jsp").include(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
