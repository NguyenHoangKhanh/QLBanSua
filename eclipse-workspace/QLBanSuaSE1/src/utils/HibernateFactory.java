package utils;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateFactory {
//	private static StandardServiceRegistry registry;
	private static SessionFactory factory;

	public static SessionFactory getsF() {
		if (factory == null) {
			StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();

			MetadataSources metadataSources = new MetadataSources(registry);

			Metadata metadata = metadataSources.getMetadataBuilder().build();

			factory = metadata.getSessionFactoryBuilder().build();

		}
		return factory;
	}
}
