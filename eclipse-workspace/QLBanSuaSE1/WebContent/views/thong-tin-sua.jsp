<%-- 
    Document   : thong-tin-sua
    Author     : hv
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="page2" class="main">
	<div class="col1 thuc-don">
		<%@include file="thuc-don.jsp"%>
	</div>
	<div class="col2">
		<table border="0" width="100%">
			<tr>
				<td colspan="2" class="tieu-de-sua">${result.tenSua}</td>
			</tr>
			<tr>
				<td><img src="./images/${result.hinh}" /></td>
				<td>
					<p>
						<b><i>Thành phần dinh dưỡng:</i></b><br>${result.tpDinhDuong}</p>
					<p>
						<b><i>Lợi ích:</i></b><br>${result.loiIch}</p>
					<p>
						<b><i>Trọng lượng:</i></b> ${result.trongLuong} - <b><i>Đơn
								giá:</i></b>${result.donGia}</p>
				</td>
			</tr>
		</table>
	</div>
</div>